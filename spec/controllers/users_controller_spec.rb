require 'spec_helper'

RSpec.describe UsersController, :type => :controller do
  include Devise::TestHelpers

  let(:user_1) { FactoryGirl.create(:user) }
  let(:user_2) { FactoryGirl.create(:user) }

  describe 'GET #index' do
    before(:each) do
      16.times do |x|
        user = FactoryGirl.create(:user)
        user.points = 1000 + x
        user.save
      end
    end

    context 'json' do
      it 'responds successfully with an application/json content' do
        get :index, { format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds successfully with an HTTP 200 status code' do
        get :index, { format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'return list of 10 users' do
        get :index, { format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.length).to eq(10)
      end

      it 'sorts users by points count' do
        get :index, { format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.first['points']).to eq(1015)
        expect(response_body.last['points']).to eq(1006)
      end
    end
  end

  describe 'GET #show' do
    context 'json' do
      it 'responds successfully with an application/json content' do
        get :show, { id: user_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds successfully with an HTTP 200 status code' do
        get :show, { id: user_1.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds with 404 if non existing id is given' do
        get :show, { id: 55, format: 'json' }
        expect(response.status).to eq(404)
      end
    end
  end

  describe 'PUT #update' do
    before(:each) do
      sign_in user_1
      @user_attributes = FactoryGirl.attributes_for(:user)
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        put :update, { id: user_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds successfully with an application/json content' do
        sign_out user_1
        put :update, { id: user_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 400 if params missing' do
        put :update, { id: user_1.id, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 401 and error if trying to update user other than logged' do
        expected_error = { error: I18n.t('common.check_resource_owner_error') }.to_json
        put :update, { id: user_2.id, user: @user_attributes, format: 'json' }
        expect(response.status).to eq(401)
        expect(response.body).to eq(expected_error)
      end

      it 'responds with 400 and errors if updated field has incorrect value' do
        updated_user = user_1.as_json
        updated_user[:name] = 'AB'
        put :update, { id: user_1.id, user: updated_user, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 200 if params correct and has rights to do so' do
        updated_user = user_1.as_json
        put :update, { id: user_1.id, user: updated_user, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'updates user if params correct and has rights to do so' do
        updated_user = user_1.as_json
        updated_user[:name] = 'The Milky Way'
        put :update, { id: user_1.id, user: updated_user, format: 'json' }
        expect(User.find(user_1.id).name).to eq(updated_user[:name])
      end
    end
  end

  describe 'POST #avatar' do
    before(:each) do
      sign_in user_1
      @user_attributes = FactoryGirl.attributes_for(:user)
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        post :avatar, { id: user_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds successfully with an application/json content' do
        sign_out user_1
        post :avatar, { id: user_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 400 if params missing' do
        post :avatar, { id: user_1.id, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 401 and error if trying to update user other than logged' do
        expected_error = { error: I18n.t('common.check_resource_owner_error') }.to_json
        post :avatar, { id: user_2.id, user: @user_attributes, format: 'json' }
        expect(response.status).to eq(401)
        expect(response.body).to eq(expected_error)
      end

      it 'responds with 400 and errors if updated field has incorrect value' do
        post :avatar, { id: user_1.id, file: 'zzz', format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'puts warning to the console if we are being hacked' do
        expect(STDOUT).to receive(:puts).with('They are hakkking us.')
        post :avatar, { id: user_1.id, file: 'zzz', format: 'json' }
      end

      it 'responds with 200 if params correct and has rights to do so' do
        file = fixture_file_upload('default.jpg', 'image/jpg')
        allow(User).to receive(:update).with(:avatar => file).and_return(true)
        post :avatar, { id: user_1.id, file: file, format: 'json' }
        expect(response.status).to eq(200)
      end
    end
  end
end