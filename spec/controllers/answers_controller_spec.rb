require 'spec_helper'

RSpec.describe AnswersController, :type => :controller do
  include Devise::TestHelpers
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:question_1) { FactoryGirl.create(:question, :user_id => user_1.id) }
  let!(:question_2) { FactoryGirl.create(:question, :user_id => user_2.id) }
  let!(:answer_attributes) { FactoryGirl.attributes_for(:answer) }

  describe 'POST #create' do
    before(:each) do
      sign_in user_1
    end

    context 'json' do
      it 'responds with 401 if user is not authenticated' do
        sign_out user_1
        post :create, { question_id: question_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with an application/json content' do
        sign_out user_1
        post :create, { question_id: question_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 201 with message is given if object is created' do
        post :create, { question_id: question_1.id, answer: answer_attributes, format: 'json' }
        expected_message = { message: I18n.t('answer.create_success') }.to_json
        expect(response.status).to eq(201)
        expect(response.body).to eq(expected_message)
      end

      it 'creates object in database if all params are correct' do
        answers_count = Answer.all.count
        post :create, { question_id: question_1.id, answer: answer_attributes, format: 'json' }
        expect(Answer.all.count).to eq(answers_count + 1)
      end

      it 'calls Mailer to deliver email later when answer is created' do
        expect(UserMailer).to receive_message_chain(:question_new_answer, :deliver_later)
        post :create, { question_id: question_1.id, answer: answer_attributes, format: 'json' }
      end

      it 'responds with 400 if params are missing' do
        post :create, { question_id: question_1.id, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 404 if non existing question_id is given' do
        post :create, { question_id: 55, answer: answer_attributes, format: 'json' }
        expect(response.status).to eq(404)
      end

      it 'responds with 400 when required param is empty' do
        answer_attributes_missing = answer_attributes
        answer_attributes_missing[:contents] = nil
        post :create, { question_id: question_1.id, answer: answer_attributes_missing, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 400 when associated question already has accepted answer' do
        accepted_answer = FactoryGirl.create(:answer, :user_id => user_2.id, :question_id => question_1.id)
        accepted_answer.accept
        post :create, { question_id: question_1.id, answer: answer_attributes, format: 'json' }
        expected_errors = { errors: [I18n.t('answer.accepted_already_exists_error')] }.to_json
        expect(response.status).to eq(400)
        expect(response.body).to eq(expected_errors)
      end

      it 'does NOT create anwser if associated question already has accepted answer' do
        accepted_answer = FactoryGirl.create(:answer, :user_id => user_2.id, :question_id => question_1.id)
        accepted_answer.accept
        answers_count = Answer.all.count
        post :create, { question_id: question_1.id, answer: answer_attributes, format: 'json' }
        expect(Answer.all.count).to eq(answers_count)
      end

      it 'responds with json message with list of errors if required param is empty' do
        answer_attributes_missing = answer_attributes
        answer_attributes_missing[:contents] = nil
        post :create, { question_id: question_1.id, answer: answer_attributes_missing, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body['errors'].length).to be(1)
      end
      
    end
  end

  describe 'GET #new' do
    context 'json' do
      it 'responds with an application/json content' do
        get :new, { question_id: question_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns json question response with answers in a list' do
        get :new, { question_id: question_1.id, format: 'json' }
        expected_response = {
            id: nil,
            user_id: nil,
            question_id: nil,
            contents: nil,
            accepted: false,
            created_at: nil,
            updated_at: nil
        }.as_json
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end

  describe 'PATCH #accept' do
    before(:each) do
      sign_in user_1
      @answer_1 = FactoryGirl.create(:answer, :question_id => question_1.id, :user_id => user_1.id)
      @answer_2 = FactoryGirl.create(:answer, :question_id => question_1.id)
      @answer_3 = FactoryGirl.create(:answer, :question_id => question_2.id)
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        patch :accept, { question_id: question_1.id, id: @answer_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds successfully with an application/json content' do
        sign_out user_1
        patch :accept, { question_id: question_1.id, id: @answer_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with HTTP 401 user tries to accept answer that belongs other user\'s question' do
        answer_3 = @answer_3.as_json
        expected_errors = { error: I18n.t('common.check_resource_owner_error') }.to_json
        patch :accept, { question_id: question_2.id, id: @answer_3.id, answer: answer_3, format: 'json' }
        expect(response.status).to eq(401)
        expect(response.body).to eq(expected_errors)
      end

      it 'responds with HTTP 403 user tries to accept own answer' do
        answer_1 = @answer_1.as_json
        answer_1[:accepted] = true
        expected_errors = { error: I18n.t('common.check_resource_owner_error') }.to_json
        patch :accept, { question_id: question_2.id, id: @answer_1.id, answer: answer_1, format: 'json' }
        expect(response.status).to eq(403)
        expect(response.body).to eq(expected_errors)
      end

      it 'updates object in database if all params are correct' do
        answer_2 = @answer_2.as_json
        answer_2[:accepted] = true
        patch :accept, { question_id: question_2.id, id: @answer_2.id, answer: answer_2, format: 'json' }
        expect(response.status).to eq(200)
        expect(Answer.find(@answer_2.id).accepted).to eq(true)
      end

      it 'calls Mailer to deliver email later when answer is accepted' do
        answer_2 = @answer_2.as_json
        answer_2[:accepted] = true
        expect(UserMailer).to receive_message_chain(:answer_accepted, :deliver_later)
        patch :accept, { question_id: question_2.id, id: @answer_2.id, answer: answer_2, format: 'json' }
      end

      it 'does NOT allow to accept answet if question already has other answer accepted' do
        answer_1 = @answer_1.as_json
        answer_1[:accepted] = true
        @answer_2.accepted = true
        @answer_2.save
        patch :accept, { question_id: question_1.id, id: @answer_1.id, answer: answer_1, format: 'json' }
        expect(Answer.find(@answer_1.id).accepted).to eq(false)
        expect(Answer.find(@answer_2.id).accepted).to eq(true)
      end

      it 'returns 400 if question already has other answer accepted' do
        answer_2 = @answer_2.as_json
        answer_2[:accepted] = true
        @answer_1.accepted = true
        @answer_1.save
        patch :accept, { question_id: question_2.id, id: @answer_2.id, answer: answer_2, format: 'json' }
        expect(response.status).to eq(400)
      end
    end
  end

end