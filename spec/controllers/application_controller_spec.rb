require 'spec_helper'

RSpec.describe ApplicationController, :type => :controller do
  include Devise::TestHelpers

  describe 'ActiveRecord::RecordNotFound' do
    controller do
      def index
        raise ActiveRecord::RecordNotFound
      end
    end

    it 'responds with 404 instead raising error' do
      get :index
      expect(response.status).to eq(404)
    end
  end

  describe 'ActionController::ParameterMissing' do
    controller do
      def index
        raise ActionController::ParameterMissing, 'test'
      end
    end

    it 'responds with 400 instead raising error' do
      get :index
      expect(response.status).to eq(400)
    end
  end

  describe 'default_format_json' do
    controller do
      def index
        respond_to do |format|
          format.json { render :json => { :who_am_i => 'I am nice JSON' } }
        end
      end
    end

    it 'returns json response even if format is not given' do
      get :index
      expect(response.status).to eq(200)
      expect(response.header['Content-Type']).to include('application/json')
    end
  end

  describe 'check_resource_owner!' do
    let(:user_1) { FactoryGirl.create(:user) }
    let(:user_2) { FactoryGirl.create(:user) }
    let(:question_1) { FactoryGirl.create(:question, :user => user_1) }
    let(:question_2) { FactoryGirl.create(:question, :user => user_2) }

    before(:each) do
      sign_in user_1
    end

    controller do
      before_action do
        check_resource_owner!(@resource_1, @resource_2, @options)
      end

      def index
        respond_to do |format|
          format.json { render :json => { :who_am_i => 'I am nice JSON' } }
        end
      end

      def set_variables(r1, r2, o)
        @resource_1 = r1
        @resource_2 = r2
        @options = o
      end
    end

    it 'does NOT generate error if user is owner of the resource (as expected by default)' do
      controller.set_variables(user_1, question_1.user, {})
      get :index
      expect(response.status).to eq(200)
    end

    it 'does NOT generate error if user is NOT owner of the resource (as expected by default)' do
      controller.set_variables(user_1, question_2.user, { :expected => false })
      get :index
      expect(response.status).to eq(200)
    end

    it 'does NOT generate response if user is owner of the resource (as expected)' do
      expect(subject.check_resource_owner!(user_1, question_2.user, :expected => false)).to eq(nil)
    end

    it 'does generate 401 error response if user is NOT owner of the resource (other than expected)' do
      controller.set_variables(user_1, question_2.user, {})
      get :index
      expect(response.status).to eq(401)
    end

    it 'does generate 401 error response if user is owner of the resource (other than expected)' do
      controller.set_variables(user_1, question_1.user, { :expected => false })
      get :index
      expect(response.status).to eq(401)
    end

    it 'does generate 403 error response if user is NOT owner of the resource (other than expected) and extra status param is given' do
      controller.set_variables(user_1, question_2.user, { :status => 403 })
      get :index
      expect(response.status).to eq(403)
    end

    it 'does generate error message if user is NOT owner of the resource (other than expected)' do
      controller.set_variables(user_1, question_2.user, {})
      expected_error = { error: I18n.t('common.check_resource_owner_error') }.to_json
      get :index
      expect(response.body).to eq(expected_error)
    end
  end
end