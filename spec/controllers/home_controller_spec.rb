require 'spec_helper'

RSpec.describe HomeController, :type => :controller do
  include Devise::TestHelpers

  describe 'GET #index' do
    it 'responds successfully with an HTTP 200 status code' do
      get :index
      expect(subject).to render_template('home/index.html.erb')
    end
  end
end