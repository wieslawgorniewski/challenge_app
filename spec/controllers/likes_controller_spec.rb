require 'spec_helper'

RSpec.describe LikesController, :type => :controller do
  include Devise::TestHelpers
  let(:user_1) { FactoryGirl.create(:user) }
  let(:user_2) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, :user => user_1) }
  let(:answer_1) { FactoryGirl.create(:answer, :user => user_1, :question => question) }
  let(:answer_2) { FactoryGirl.create(:answer, :user => user_2, :question => question) }

  describe 'GET #index' do
    before(:each) do
      FactoryGirl.create(:like, :user => user_1, :answer => answer_1)
      FactoryGirl.create(:like, :user => user_2, :answer => answer_1)
      FactoryGirl.create(:like, :user => user_1, :answer => answer_2)
    end

    context 'json' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, { answer_id: answer_1.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds successfully with an application/json content' do
        get :index, { answer_id: answer_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns list of all likes associated with answer' do
        get :index, { answer_id: answer_1.id, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.length).to eq(2)
      end
    end
  end

  describe 'GET #index_user_likes' do
    before(:each) do
      FactoryGirl.create(:like, :user => user_1, :answer => answer_1)
      FactoryGirl.create(:like, :user => user_2, :answer => answer_1)
      FactoryGirl.create(:like, :user => user_1, :answer => answer_2)
    end

    context 'json' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index_user_likes, { user_id: user_2.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds successfully with an application/json content' do
        get :index_user_likes, { user_id: user_2.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns list of all likes associated with answer' do
        get :index_user_likes, { user_id: user_2.id, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.length).to eq(1)
      end
    end
  end

  describe 'GET #new' do
    context 'json' do
      it 'responds successfully with an HTTP 200 status code' do
        get :new, { answer_id: answer_1.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds successfully with an application/json content' do
        get :new, { answer_id: answer_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'renturns new json like with empty values' do
        like_attributes = Like.new.as_json
        get :new, { answer_id: answer_1.id, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body).to eq(like_attributes)
      end
    end
  end

  describe 'POST #create' do
    before(:each) do
      sign_in user_1
      @like_attributes = Like.new.as_json
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        post :create, { answer_id: answer_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with an application/json content' do
        sign_out user_1
        post :create, { answer_id: answer_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 201 and message if object is created' do
        post :create, { answer_id: answer_2.id, format: 'json' }
        expected_message = { message: I18n.t('like.create_success') }.to_json
        expect(response.status).to eq(201)
        expect(response.body).to eq(expected_message)
      end

      it 'creates object in database if all params are correct' do
        questions_count = Like.all.count
        post :create, { answer_id: answer_2.id, format: 'json' }
        expect(Like.all.count).to eq(questions_count + 1)
      end

      it 'responds with 400 when like already exists' do
        FactoryGirl.create(:like, :user => user_1, :answer => answer_2)
        post :create, { answer_id: answer_2.id, format: 'json' }
        expected_message = { message: I18n.t('like.create_success') }.to_json
        expect(response.status).to eq(400)
        expect(JSON.parse(response.body)).to have_key('errors')
      end

      it 'responds with 403 and error if user tries to like own answer' do
        post :create, { answer_id: answer_1.id, format: 'json' }
        expected_message = { error: I18n.t('common.check_resource_owner_error') }.to_json
        expect(response.status).to eq(403)
        expect(response.body).to eq(expected_message)
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      sign_in user_1
      @like_1 = FactoryGirl.create(:like, :user => user_1, :answer => answer_1)
      @like_2 = FactoryGirl.create(:like, :user => user_2, :answer => answer_1)
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        delete :destroy, { answer_id: answer_1.id, id: @like_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with 401 if authenticated user is not owner of the resource' do
        delete :destroy, { answer_id: answer_1.id, id: @like_2.id, format: 'json' }
        expected_message = { errors: I18n.t('like.delete_error') }.to_json
        expect(response.status).to eq(401)
        expect(response.body).to eq(expected_message)
      end

      it 'does NOT delete resource if authenticated user is not the owner' do
        questions_count = Like.all.count
        delete :destroy, { answer_id: answer_1.id, id: @like_2.id, format: 'json' }
        expect(Like.all.count).to eq(questions_count)
      end

      it 'responds with 204 and message is given if object is deleted' do
        delete :destroy, { answer_id: answer_1.id, id: @like_1.id, format: 'json' }
        expected_message = { message: I18n.t('like.delete_success') }.to_json
        expect(response.status).to eq(204)
        expect(response.body).to eq(expected_message)
      end

      it 'deletes object from database if all params are correct' do
        questions_count = Like.all.count
        delete :destroy, { answer_id: answer_1.id, id: @like_1.id, format: 'json' }
        expect(Like.all.count).to eq(questions_count - 1)
        expect(Like.exists?(@like_1.id)).to be false
        expect(Like.exists?(@like_2.id)).to be true
      end
    end
  end
end