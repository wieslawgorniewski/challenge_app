require 'spec_helper'

RSpec.describe QuestionsController, :type => :controller do
  include Devise::TestHelpers
  let(:user_1) { FactoryGirl.create(:user) }
  let(:user_2) { FactoryGirl.create(:user) }

  before(:each) do
    @question_1 = FactoryGirl.create(:question, :user_id => user_1.id)
    @question_2 = FactoryGirl.create(:question, :user_id => user_2.id, :contents => 'Funny question.')
    @answer_1 = FactoryGirl.create(
        :answer,
        :user_id => user_2.id,
        :question_id => @question_1.id
    )
    @answer_2 = FactoryGirl.create(
        :answer,
        :user_id => user_2.id,
        :question_id => @question_1.id,
        :contents => 'Even better answer than previous one'
    )
  end

  describe 'GET #index' do
    context 'json' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, { format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds successfully with an application/json content' do
        get :index, { format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns list of all questions' do
        get :index, { format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.length).to eq(2)
        expect(response_body[0]['title']).to eq(@question_1.title)
        expect(response_body[0]['user_id']).to eq(user_1.id)
        expect(response_body[1]['title']).to eq(@question_2.title)
        expect(response_body[1]['user_id']).to eq(user_2.id)
      end

      it 'includes user with name attribute to all questions' do
        user_1.name = 'TestName'
        user_1.save
        get :index, { format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body.length).to eq(2)
        expect(response_body[0]['user']).to eq({'name'=> 'TestName'})
        expect(response_body[1]['user']).to eq({'name'=> nil})
      end
    end
  end

  describe 'GET #show' do
    context 'json' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds with 404 if non existing id is given' do
        get :show, { id: 55, format: 'json' }
        expect(response.status).to eq(404)
      end

      it 'responds successfully with an application/json content' do
        get :show, { id: @question_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns json question response with answers in a list' do
        get :show, { id: @question_1.id, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body['id']).to eq(@question_1.id)
        expect(response_body['answers'].length).to eq(2)
        expect(response_body['answers'][0]['contents']).to eq(@answer_1.contents)
        expect(response_body['answers'][1]['contents']).to eq(@answer_2.contents)
        expect(response_body['answers'][0]['likes_count']).to eq(@answer_1.likes_count)
      end

      it 'returns json question response with user containing only name attribute' do
        user_1.name = 'TestName'
        user_1.save
        get :show, { id: @question_1.id, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body['id']).to eq(@question_1.id)
        expect(response_body['user']['name']).to eq('TestName')
        expect(response_body['user']['avatar_thumb_url']).to include('default.jpg')
        expect(response_body['user'].size).to eq(2)
      end
    end
  end

  describe 'GET #new' do
    context 'json' do
      it 'responds with an application/json content' do
        get :new, { format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'returns json question response with answers in a list' do
        sign_in user_1
        get :new, { format: 'json' }
        expected_response = {
            id: nil,
            title: nil,
            user_id: nil,
            contents: nil,
            created_at: nil,
            updated_at: nil
        }.as_json
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end

  describe 'GET #edit' do
    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        get :edit, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with an application/json content' do
        get :edit, { id: @question_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with HTTP 200 if user is authenticated' do
        sign_in user_1
        get :edit, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(200)
      end

      it 'responds with HTTP 401 if user is not authenticated' do
        get :edit, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end
    end
  end

  describe 'POST #create' do
    before(:each) do
      sign_in user_1
      get :new, { format: 'json' }
      @question_attributes = FactoryGirl.attributes_for(:question)
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        post :create, { format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with an application/json content' do
        sign_out user_1
        post :create, { format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 201 and message if object is created' do
        post :create, { question: @question_attributes, format: 'json' }
        expected_message = { message: I18n.t('question.create_success') }.to_json
        expect(response.status).to eq(201)
        expect(response.body).to eq(expected_message)
      end

      it 'creates object in database if all params are correct' do
        questions_count = Question.all.count
        post :create, { question: @question_attributes, format: 'json' }
        expect(Question.all.count).to eq(questions_count + 1)
      end

      it 'responds with 400 when required param is empty' do
        question_attributes_missing = @question_attributes
        question_attributes_missing[:title] = nil
        post :create, { question: question_attributes_missing, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with json message with list of errors if required param is empty' do
        question_attributes_missing = @question_attributes
        question_attributes_missing[:title] = nil
        post :create, { question: question_attributes_missing, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body['errors'].length).to be(1)
      end

      it 'responds with 400 if params are missing' do
        post :create, { format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'responds with 400 when user has not enough points to create question' do
        user_1.points = User::POINTS_QUESTION_COST - 1
        user_1.save
        post :create, { question: @question_attributes, format: 'json' }
        expected_errors = { errors: [I18n.t('question.not_enough_points_error')] }.to_json
        expect(response.status).to eq(400)
        expect(response.body).to eq(expected_errors)
      end
    end
  end

  describe 'PUT #update' do
    before(:each) do
      sign_in user_1
      @question_to_update = @question_1.as_json
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        put :update, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds successfully with an application/json content' do
        sign_out user_1
        put :update, { id: @question_1.id, format: 'json' }
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'responds with 400 if params missing' do
        put :update, { id: @question_1.id, format: 'json' }
        expect(response.status).to eq(400)
      end

      it 'updates object in database if all params are correct' do
        @question_to_update['title'] = 'Updated title'
        put :update, { id: @question_1.id, question: @question_to_update, format: 'json' }
        expect(Question.find(@question_to_update['id']).title).to eq(@question_to_update['title'])
      end

      it 'responds with errors if object is not updated' do
        @question_to_update['title'] = nil
        put :update, { id: @question_1.id, question: @question_to_update, format: 'json' }
        response_body = JSON.parse(response.body)
        expect(response_body['errors'].length).to be(1)
      end

      it 'responds with 401 if user does not have right to modify resource' do
        put :update, { id: @question_2.id, question: @question_2.as_json, format: 'json' }
        expected_errors = { error: I18n.t('common.check_resource_owner_error') }.to_json
        expect(response.body).to eq(expected_errors)
        expect(response.status).to eq(401)
      end

      it 'does not update resource if user has no ownership right' do
        question_2_to_update = @question_2.as_json
        question_2_to_update['title'] = 'New Title'
        put :update, { id: @question_2.id, question: @question_2.as_json, format: 'json' }
        expect(Question.find(@question_2.id).title).not_to eq(question_2_to_update['title'])
        expect(Question.find(@question_2.id).title).to eq(@question_2.title)
      end

      it 'gives message if object is updated' do
        @question_to_update['title'] = 'Updated title'
        put :update, { id: @question_1.id, question: @question_to_update, format: 'json' }
        expected_message = { message: I18n.t('question.update_success') }.to_json
        expect(response.body).to eq(expected_message)
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      sign_in user_1
    end

    context 'json' do
      it 'responds with HTTP 401 status code if user is not authenticated' do
        sign_out user_1
        delete :destroy, { id: 1, format: 'json' }
        expect(response.status).to eq(401)
      end

      it 'responds with 204 and message is given if object is deleted' do
        delete :destroy, { id: 1, format: 'json' }
        expected_message = { message: I18n.t('question.delete_success') }.to_json
        expect(response.status).to eq(204)
        expect(response.body).to eq(expected_message)
      end

      it 'deletes object from database if all params are correct' do
        questions_count = Question.all.count
        delete :destroy, { id: 1, format: 'json' }
        expect(Question.all.count).to eq(questions_count - 1)
        expect(Question.exists?(1)).to be false
        expect(Question.exists?(2)).to be true
      end

      it 'responds with 401 if user does not have right to destroy resource' do
        delete :destroy, { id: @question_2.id, format: 'json' }
        expected_errors = { error: I18n.t('common.check_resource_owner_error') }.to_json
        expect(response.body).to eq(expected_errors)
        expect(response.status).to eq(401)
      end

      it 'does not destroy resource if user has no ownership right' do
        count = Question.all.count
        delete :destroy, { id: @question_2.id, format: 'json' }
        expect(Question.all.count).to eq(count)
      end
    end
  end
end