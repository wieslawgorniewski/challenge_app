require 'spec_helper'

RSpec.describe Like, :type => :model do
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:question_1) { FactoryGirl.create(:question, :user => user_1) }
  let!(:answer_1) { FactoryGirl.create(:answer, :question => question_1, :user => user_2) }

  describe '#reward_answer_creator_with_points' do
    it 'adds points to user that gave liked answer' do
      expect(user_2).to receive(:add_points).with(User::POINTS_LIKE)
      Like.create(:user => user_1, :answer => answer_1)
    end
  end

  describe '#take_points_away' do
    it 'removes points from user that gave unliked answer' do
      @like = Like.create(:user => user_1, :answer => answer_1)
      expect(user_2).to receive(:remove_points).with(User::POINTS_LIKE)
      @like.destroy
    end
  end
end