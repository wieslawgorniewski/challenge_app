require 'spec_helper'

RSpec.describe Answer, :type => :model do
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:question_1) { FactoryGirl.create(:question, :user => user_1) }
  let!(:question_2) { FactoryGirl.create(:question, :user => user_1) }
  let!(:answer_1) { FactoryGirl.create(:answer, :question => question_1, :user => user_1) }
  let!(:answer_2) { FactoryGirl.create(:answer, :question => question_1, :user => user_1) }
  let!(:answer_3) { FactoryGirl.create(:answer, :question => question_2, :user => user_1) }

  describe '#likes_count' do
  	it 'returns 0 for answer without likes' do
  		expect(Answer.find(answer_1.id).likes_count).to eq(0)
  	end

  	it 'returns 2 for answer without likes' do
      FactoryGirl.create(:like, :answer => answer_2, :user => user_1)
      FactoryGirl.create(:like, :answer => answer_2, :user => user_2)
      expect(Answer.find(answer_2.id).likes_count).to eq(2)
    end
  end

  describe '#accept' do
    it 'does NOT allow to accept answer if other has already been accepted' do
      answer_1.update(:accepted => true)
      answer_2.accept
      expect(Answer.find(answer_1.id).accepted).to eq(true)
      expect(Answer.find(answer_2.id).accepted).to eq(false)
    end

    it 'does NOT change answers related with other questions' do
      answer_1.update(:accepted => true)
      answer_3.update(:accepted => true)
      answer_1.accept
      expect(Answer.find(answer_1.id).accepted).to eq(true)
      expect(Answer.find(answer_3.id).accepted).to eq(true)
    end

    it "adds points to user that gave accepted answer" do
      expect(answer_1.user).to receive(:add_points).with(User::POINTS_ANSWER)
      answer_1.accept
    end
  end

  describe '#set_as_not_accepted' do
    it 'by default sets accepted to false after create' do
      answer = Answer.new(:user => user_1, :question => question_1, :accepted => true, :contents => 'abc')
      answer.save
      expect(Answer.last.accepted).to eq(false)
    end
  end

  describe '#allow_create_if_question_has_no_answers' do
    it 'does NOT allow to create if question already has accepted answer' do
      answers_count = Answer.all.count
      answer_1.accept
      new_answer = Answer.new(:user => user_2, :question => answer_1.question, :contents => 'abc')
      new_answer.save
      expect(Answer.all.count).to eq(answers_count)
    end

    it 'adds erros to the instance if ascociated question already has accepted answer' do
      answer_1.accept
      new_answer = Answer.new(:user => user_2, :question => answer_1.question, :contents => 'abc')
      new_answer.save
      expect(new_answer.errors.full_messages).to eq([I18n.t('answer.accepted_already_exists_error')])
    end

    it 'allows to create if question does not have accepted answer' do
      answers_count = Answer.all.count
      new_answer = Answer.new(:user => user_2, :question => answer_1.question, :contents => 'abc')
      new_answer.save
      expect(Answer.all.count).to eq(answers_count+1)
    end
  end
end