require 'spec_helper'

RSpec.describe User, :type => :model do
  before(:each) do
    @user = User.create(:email => 'tester@tester.pl', :password => 'topSecret')
  end

  describe 'as_json' do
    it "returns extra attributes in json serialized model" do
      expect(User.last.as_json).to include(:superstar_badge, :avatar_thumb_url)
    end
  end

  describe '#superstar_badge' do
    it "returns true if user has more or equal to #{User::POINTS_SUPERSTAR} points" do
      User.last.update(:points => User::POINTS_SUPERSTAR)
      expect(User.last.superstar_badge).to eq(true)
    end

    it "returns false if user has less than #{User::POINTS_SUPERSTAR} points" do
      User.last.update(:points => User::POINTS_SUPERSTAR - 1)
      expect(User.last.superstar_badge).to eq(false)
    end
  end

  describe '#avatar_thumb_url' do
    it "returns user avatar thumb url" do
      expect(User.last.avatar_thumb_url).to include('thumb/default.jpg')
    end
  end

  describe '#set_initial_points' do
    it "gives user #{User::POINTS_INITIAL_BONUS} points on create" do
      expect(User.last.points).to eq(User::POINTS_INITIAL_BONUS)
    end

    it "does NOT give user any points if save is not initial" do
      @user.email = 'new_email@tester.pl'
      @user.save
      expect(User.last.points).to eq(User::POINTS_INITIAL_BONUS)
    end
  end

  describe '#add_points' do
    it 'adds points to user points atrribute' do
      points_initial = User.last.points
      @user.add_points(71)
      expect(User.last.points).to eq(points_initial + 71)
    end
  end

  describe '#remove points' do
    it 'remove points from user points atrribute' do
      points_initial = User.last.points
      @user.remove_points(20)
      expect(User.last.points).to eq(points_initial - 20)
    end

    it 'prevents from points atrribute to go below 0' do
      points_initial = User.last.points
      @user.remove_points(points_initial + 1)
      expect(User.last.points).to eq(points_initial)
    end
  end
end