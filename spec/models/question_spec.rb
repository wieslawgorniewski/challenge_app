require 'spec_helper'

RSpec.describe Question, :type => :model do
  let!(:user_1) { FactoryGirl.create(:user) }

  describe '#create_if_user_has_enough_points' do
    it 'allows to create question if user has enough points' do
      questions_count = Question.all.count
      Question.create(:user => user_1, :title => 'Very nice title', :contents => 'Even nicer content.')
      expect(Question.all.count).to eq(questions_count + 1)
    end

    it 'allows to create question if user has enough points' do
      user_1.points = User::POINTS_QUESTION_COST - 1
      questions_count = Question.all.count
      Question.create(:user => user_1, :title => 'Very nice title', :contents => 'Even nicer content.')
      expect(Question.all.count).to eq(questions_count)
    end

    it 'adds erros to the instance if ascociated user has not enough points' do
      user_1.points = User::POINTS_QUESTION_COST - 1
      questions_count = Question.all.count
      question = Question.create(:user => user_1, :title => 'Very nice title', :contents => 'Even nicer content.')
      expect(question.errors.full_messages).to eq([I18n.t('question.not_enough_points_error')])
    end
  end

  describe '#remove_user_points' do
    it 'is called if user has enough points to create question' do
      expect(user_1).to receive(:remove_points).with(User::POINTS_QUESTION_COST)
      Question.create(:user => user_1, :title => 'Very nice title', :contents => 'Even nicer content.')
    end

    it 'is NOT called if user does NOT have enough points to create question' do
      user_1.points = User::POINTS_QUESTION_COST - 1
      expect(user_1).not_to receive(:remove_points).with(User::POINTS_QUESTION_COST)
      Question.create(:user => user_1, :title => 'Very nice title', :contents => 'Even nicer content.')
    end
  end
end