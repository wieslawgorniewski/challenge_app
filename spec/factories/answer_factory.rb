FactoryGirl.define do
  factory :answer do
    contents 'Best answer ever'
    accepted false
    question
    user
  end
end
