require 'spec_helper'

RSpec.describe UserMailer do
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:question_1) { FactoryGirl.create(:question, :user_id => user_1.id) }
  let!(:answer_1) { FactoryGirl.create(:answer, :user => user_1, :question => question_1) }

  before(:each) do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  after(:each) do
    ActionMailer::Base.deliveries.clear
  end

  describe '#question_new_answer' do
    it 'sends one email' do
      UserMailer.question_new_answer(question_1).deliver_now
      expect(ActionMailer::Base.deliveries.count).to eq(1)
    end

    it 'sends email with correct subject' do
      UserMailer.question_new_answer(question_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].subject).to eq(I18n.t('question.email.new_answer_title'))
    end

    it 'sends email to correct body' do
      UserMailer.question_new_answer(question_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].body).to include(question_1.title)
    end

    it 'sends email to correct user' do
      UserMailer.question_new_answer(question_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].to.count).to eq(1)
      expect(ActionMailer::Base.deliveries[0].to[0]).to eq(question_1.user.email)
    end
  end

  describe '#answer_accepted' do
    it 'sends one email' do
      UserMailer.answer_accepted(answer_1).deliver_now
      expect(ActionMailer::Base.deliveries.count).to eq(1)
    end

    it 'sends email with correct subject' do
      UserMailer.answer_accepted(answer_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].subject).to eq(I18n.t('answer.email.answer_accepted_title'))
    end

    it 'sends email to correct body' do
      UserMailer.answer_accepted(answer_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].body).to include(I18n.t('answer.email.answer_accepted_line_1'))
    end

    it 'sends email to correct user' do
      UserMailer.answer_accepted(answer_1).deliver_now
      expect(ActionMailer::Base.deliveries[0].to.count).to eq(1)
      expect(ActionMailer::Base.deliveries[0].to[0]).to eq(answer_1.user.email)
    end
  end
end