require 'spec_helper'
require 'rake'

describe 'add_initial_bonus_points_to_existing_users' do
  before do
    load File.expand_path(Rails.root.join("lib/tasks/challange_tasks.rake").to_s, __FILE__)
    Rake::Task.define_task(:environment)
  end

  before(:each) do
    3.times do
      user = FactoryGirl.create(:user)
      user.points = nil
      user.save
    end

    [64, 0].each do |points|
      user = FactoryGirl.create(:user)
      user.points = points
      user.save
    end
  end

  it "adds 100 points to the users that have points equal to nil" do
  	Rake::Task["add_initial_bonus_points_to_existing_users"].execute
    expect(User.where(:points => nil).count).to eq(0)
    expect(User.where(:points => User::POINTS_INITIAL_BONUS).count).to eq(3)
  end

  it "does NOT add points to the users that have any other value of points than nil" do
  	Rake::Task["add_initial_bonus_points_to_existing_users"].execute
    expect(User.where(:points => 0).count).to eq(1)
    expect(User.where(:points => 64).count).to eq(1)
  end
end