# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user_data = [
	{
		:id => 1,
		:name => 'Steve',
		:email => 'test1@test.com',
		:cheating_points => 4321
	},
	{
		:id => 2,
		:name => nil,
		:email => 'test2@test.com'
	},
	{
		:id => 3,
		:name => 'Johnny',
		:email => 'test3@test.com'
	}
]

questions_data = [
	{
		:title => 'Mass of Jupiter and Saturn.',
		:contents => 'Is mass of Jupiter greater than mass of Saturn?',
		:user_id => 2,
		:answers => []
	},
	{
		:title => 'Why are dumplings so popular in Poland?',
		:contents => '',
		:user_id => 3,
		:answers => [
			{
				:contents => 'Anyone?',
				:user_id => 3
			},
			{
				:contents => 'Because pierogi are good.',
				:user_id => 3
			}
		]
	},
	{
		:title => 'Estamined time to get to the Moon.',
		:contents => 'How long will my trip take?',
		:user_id => 1,
		:answers => [
			{
				:contents => 'About 3 days with Saturn V rocket, but if it breaks it might take longer.',
				:user_id => 2
			}
		]
	},
	{
		:title => 'Should I wear blue or red jacket?',
		:contents => 'No flame please.',
		:user_id => 1,
		:answers => [
			{
				:contents => 'They are about the same. Use both.',
				:user_id => 3
			},
			{
				:contents => 'Long answer. Very. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Long answer. Very. Long answer. Very. Long answer. Very. Long answer. Very. Long answer. Very. Long answer. Very. Long answer. Very.',
				:user_id => 2
			},
			{
				:contents => 'Supercalifragilisticexpialidocious.',
				:user_id => 1
			}
		]
	}
]

user_data.each do |user|
  u = User.create!(
	:id => user[:user_id],
	:email => user[:email],
	:password => 'secret123',
	:password_confirmation => 'secret123'
  )
  u.name = user[:name] if user[:name]
  u.points = user[:cheating_points] if user[:cheating_points]
  u.save
end

questions_data.each do |question|
  q = Question.create!(
	:title => question[:title],
	:contents => question[:contents],
	:user_id => question[:user_id]
  )
  question[:answers].each do |answer|
  	Answer.create!(
  		:contents => answer[:contents],
  		:user_id => answer[:user_id],
  		:question_id => q.id
  	)
  end
end