module WaitForAjax
  def wait_for_ajax_until(condition)
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until condition
    end
  end
end