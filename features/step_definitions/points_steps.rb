Then(/^I should have (\d+) points$/) do |points|
  sleep(2.seconds) # Ugly, veeeerrrry ugly
  expect(@user.reload.points).to be(points.to_i)
end

Given(/^I have (\d+) points$/) do |points|
  @user.points = points
  @user.save!
end

Then(/^Other user should have (\d+) points$/) do |points|
  sleep(2.seconds) # Even uglier than the one above
  user = User.find(@answer.reload.user.id)
  expect(user.points).to eq(points.to_i)
end

