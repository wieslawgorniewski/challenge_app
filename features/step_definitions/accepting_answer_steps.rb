When(/^I accept this answer$/) do
  visit("/#/questions/#{@answer.question.id}/")

  WaitForAjax::wait_for_ajax_until(find('a', text: 'accept'))
  within "#answer-#{@answer.id}" do
    find('a', text: 'accept').click
  end
  WaitForAjax::wait_for_ajax_until(find('a', text: 'accepted'))
end

Then(/^That answer should be marked as accepted$/) do
  visit("/#/questions/#{@answer.question.id}/")

  WaitForAjax::wait_for_ajax_until(find('a', text: 'accepted'))
  within "#answer-#{@answer.id}" do
    expect(page).to have_content("accepted")
  end
end

Then(/^It should not be possible to add more answers$/) do
  visit("/#/questions/#{@answer.question.id}/")
  
  expect(page).not_to have_content("Add answer")
end

