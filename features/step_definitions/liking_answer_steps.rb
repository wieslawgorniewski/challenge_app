Then(/^That answer should have (\d+) like(s?)$/) do |count, suffix|
  within :id, "answer-#{@answer.id}" do
    expect(page).to have_content("#{count} like#{suffix}")
  end
end

When(/^I like this answer$/) do
  visit("/#/questions/#{@answer.question.id}")
  WaitForAjax::wait_for_ajax_until(find('a', text: '0 likes'))
  within :id, "answer-#{@answer.id}" do
    find('a', text: 'likes').click
  end
end

When(/^This answer is accepted$/) do
  sign_in_as(@answer.question.user)
  visit question_path(@answer.question)
  within "answer-#{@answer.id}" do
    click_on "Accept"
  end

  expect(@user.reload.points).to be(125)
end
