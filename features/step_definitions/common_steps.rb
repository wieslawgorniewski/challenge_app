And(/^I should see the error message: (.*)$/) do |error_message|
  expect(page).to have_content(error_message)
end

