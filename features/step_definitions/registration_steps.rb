require 'securerandom'

When(/^I create a profile$/) do
  email = "email#{SecureRandom.hex(12)}@example.com"

  visit '/'

  find_by_id('sign-up-button').click
  fill_in "Email", with: email
  fill_in "Email (confirm)", with: email
  fill_in "Password", with: "password"
  fill_in "Password (confirm)", with: "password"
  find_by_id('sign-up-submit').click

  WaitForAjax::wait_for_ajax_until(find('a', text: email))

  @user = User.find_by(email: email)
end

