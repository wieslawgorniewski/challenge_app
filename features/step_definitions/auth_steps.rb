Given(/^I am user$/) do
  email = 'tester@tester.pl'
  password = 'topSecret'
  @user = User.create(:email => email, :password => password)

  visit '/'

  find_by_id('sign-in-button').click
  fill_in "Email", with: email
  fill_in "Password", with: password
  find_by_id('sign-in-submit').click

  WaitForAjax::wait_for_ajax_until(find('a', text: email))
end
