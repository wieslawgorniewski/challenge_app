When(/^I create a question$/) do
  visit '/'

  click_button 'Add question'
  fill_in "questionTitle", with: "A simple question"
  fill_in "questionContents", with: "Long question description."
  click_button "Create question"
end

When(/^I create a question with empty title$/) do
  visit '/'
  
  click_button 'Add question'
  fill_in "questionTitle", with: "We will remove this anyway in next line" # this is needed for '(This filed is required)' to occour in Angular Form
  fill_in "questionTitle", with: ""
  fill_in "questionContents", with: "Long question description."
end

Then(/^I should see this question$/) do
  WaitForAjax::wait_for_ajax_until(!find("body").has_css?("modal-open"))
  expect(page).to have_content "A simple question"
  expect(page).to have_content "Asked by user"
end

Then(/^I should not see that I can not submit form$/) do
  expect(page).to have_button('Create question', disabled: true)
end

Given(/^There is a question$/) do
  @question = create(:question)
end

Given(/^There is a question created by another user$/) do
  user = create(:user)
  @question = create(:question, :user => user)
end

Then(/^I do not see "(.*?)" link on this question's page$/) do |link|
  visit("/#/questions/#{@question.id}")
  expect(page).not_to have_button(link)
end

Given(/^I created a question$/) do
  @question = create(:question, user: @user)
end

When(/^I am on question's page$/) do
  visit question_path(@question)
end
