When(/^I answer this question$/) do
  visit("/#/questions/#{@question.id}/")
  
  WaitForAjax::wait_for_ajax_until(find_by_id('add-answer'))
  find_by_id('add-answer').click
  fill_in "answerContents", with: "This is the answer."
  click_button "Create answer"
  WaitForAjax::wait_for_ajax_until(!find("body").has_css?("modal-open"))
end

When(/^I answer this question with empty contents$/) do
  visit("/#/questions/#{@question.id}/")

  WaitForAjax::wait_for_ajax_until(find_by_id('add-answer'))
  find_by_id('add-answer').click
  fill_in "answerContents", with: "This is the answer."
  fill_in "answerContents", with: "" # this is needed for '(This filed is required)' to occour in Angular Form
end

Then(/^I should see the answer on question's page$/) do
  expect(page).to have_content("This is the answer")
  expect(page).to have_content("0 likes")
end

Then(/^I should not see the answer on question's page$/) do
  expect(page).not_to have_content("This is the answer")
  expect(page).not_to have_content("0 likes")
end

And(/^There is answer to that question$/) do
  @answer = create(:answer, question: @question)
end

And(/^There is a question with answer$/) do
  @answer = create(:answer)
end

And(/^Other user added an answer$/) do
  @other_user = create(:user)
  @question = create(:question)
  @question.user = User.find(1)
  @question.save
  @answer = create(:answer, :user => @other_user, :question => @question)
end
