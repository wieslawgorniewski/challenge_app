controllers.controller('SessionsController', function ($rootScope, $scope, $window, $route, $routeParams, $location, Api, SharedData, Auth) {
    var configPost = { headers: { 'X-HTTP-Method-Override': 'POST' } };
    var configDelete = { headers: { 'X-HTTP-Method-Override': 'DELETE' } };
    $scope.headerTemplate = 'header.html';
    $scope.signInModalTemplate = 'sign_in_modal.html';
    $scope.signUpModalTemplate = 'sign_up_modal.html';
    $scope.credentials = {
        email: null,
        emailConfirmation: null,
        password: null,
        passwordConfirmation: null
    };
    $scope.formError = null;
    $scope.formErrors = null;
    $scope.sharedData = SharedData;

    $scope.setSignedUser = function () {
        Auth.currentUser().then(function(user) {
            $scope.sharedData.setSignedUser(user);
        }, function(error) {
            $scope.sharedData.setSignedUser(null);
        });
    };

    $scope.signIn = function() {
        Auth.login($scope.credentials, configPost).then(function(user) {
            console.log('login success');
        }, function(error) {
            $scope.formError = error.data.error;
        });
    };

    $scope.signOut = function() {
        Auth.logout(configDelete).then(function(oldUser) {
            console.log('logout success');
        }, function(error) {
            console.log(error);
        });
    };

    $scope.signUp = function() {
        var credentials = angular.copy($scope.credentials);
        delete credentials.emailConfirmation;
        delete credentials.passwordConfirmation;
        Auth.register(credentials, configPost).then(function(registeredUser) {
            console.log('register success');
        }, function(error) {
            $scope.formErrors = error.data.errors;
        });
    };

    $scope.$on('devise:new-session', function(event, currentUser) {
        $scope.sharedData.setSignedUser(currentUser);
        $window.location.reload();
    });

    $scope.$on('devise:logout', function(event, oldCurrentUser) {
        $scope.sharedData.setSignedUser(null);
        $window.location.reload();
    });

    $scope.$on('devise:new-registration', function(event, user) {
        $scope.sharedData.setSignedUser(user);
        $window.location.reload();
    });
});