controllers.controller('LikesController', function ($scope, $routeParams, $window, $location, Api, SharedData) {
	$scope.sharedData = SharedData;
	$scope.allLikesFromCurrentUser = [];
    $scope.allAnwswersLikedByUserIds = [];

	$scope.userLiked = function(answer) {
		if (answer.likes_count > 0 && $scope.allAnwswersLikedByUserIds.indexOf(answer.id) > -1) {
            return true;
		}
		return false;
	}

    $scope.getLikeId = function(answer_id) {
        var like_id = -1;
        angular.forEach($scope.allLikesFromCurrentUser, function(like) {
            if (like.answer_id == answer_id) {
                like_id = like.id;
            }
        }, null);
        return like_id;
    }

 	$scope.getAllLikesFromCurrentUser = function () {
        if (!$scope.sharedData.isSigned()) {
            return;
        }
 		Api.UserLikes.query({user_id: SharedData.getSignedUser().id}, {},
            function(response, headers) {
                $scope.allLikesFromCurrentUser = response;
                $scope.allAnwswersLikedByUserIds = [];
                angular.forEach($scope.allLikesFromCurrentUser, function(like) {
                    $scope.allAnwswersLikedByUserIds.push(like.answer_id);
                }, null);             
            },
            function(response) {
                $scope.formErrors = response.data.errors;
            }
        );
 	}

    $scope.createOrDeleteLike = function (answer) {
        if (!$scope.sharedData.isSigned() || ($scope.sharedData.getSignedUser().id == answer.user_id)) {
            return;
        }
        if ($scope.userLiked(answer)) {
            $scope.destroyLike(answer, $scope.getLikeId(answer.id));
        } else {
            $scope.createLike(answer);
        }
    };

    $scope.createLike = function (answer) {
        Api.AnswerLikes.save({answer_id: answer.id}, {},
            function(response, headers) {
                $scope.getAllLikesFromCurrentUser();
                answer.likes_count += 1;
            },
            function(response) {
                $scope.formErrors = response.data.errors;
            }
        );
    };

    $scope.destroyLike = function (answer, like_id) {
        Api.AnswerLikes.remove({answer_id: answer.id, id: like_id}, {},
            function(response, headers) {
                $scope.getAllLikesFromCurrentUser();
                answer.likes_count -= 1;
            },
            function(response) {
                $scope.formErrors = response.data.errors;
            }
        );
    };
});