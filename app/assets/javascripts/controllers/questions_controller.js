controllers.controller('QuestionsController', function ($rootScope, $scope, $routeParams, $window, $location, Api, SharedData) {
    $scope.questionModalTemplate = 'question_modal.html';
    $scope.questionModalTitle = '';
    $scope.questions = (!$routeParams.id) ? Api.Questions.query() : [];
    $scope.question = ($routeParams.id) ? Api.Questions.get({ id: $routeParams.id }) : Api.Questions.get({ param: 'new' });
    $scope.formError = null;
    $scope.formErrors = null;
    $scope.sharedData = SharedData;

    $scope.createOrUpdateQuestion = function () {
        if ($scope.question.id) {
            $scope.updateQuestion();
        } else {
            $scope.createQuestion();
        }
    }

    $scope.createQuestion = function () {
        Api.Questions.save($scope.question,
            function(response, headers) {
                $window.location.reload();
            },
            function(response) {
                $scope.formError = response.data.error;
                $scope.formErrors = response.data.errors;
            }
        );
    };

    $scope.updateQuestion = function () {
        Api.Questions.update({ id: $routeParams.id }, $scope.question,
            function(response, headers) {
                $window.location.reload();
            },
            function(response) {
                $scope.formError = response.data.error;
                $scope.formErrors = response.data.errors;
            }
        );
    };

    $scope.deleteQuestion = function () {
        Api.Questions.remove({ id: $routeParams.id },
            function(response, headers) {
                $location.path('/');
            },
            function(response) {
                console.log(response);
            }
        );
    };

    $scope.isOwner = function() {
        return $scope.sharedData.getSignedUser().id == $scope.question.user_id;
    }
});