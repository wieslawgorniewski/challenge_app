controllers.controller('AnswersController', function ($scope, $route, $routeParams, $window, $location, $http, Api, SharedData) {
    $scope.newAnswerTemplate = 'new_answer_modal.html';
    $scope.answer = {
        contents: null
    };
    $scope.formErrors = null;
    $scope.sharedData = SharedData;

    $scope.createAnswer = function () {
        Api.Answers.save({question_id: $routeParams.id}, $scope.answer,
            function(response, headers) {
                $window.location.reload();
            },
            function(response) {
                $scope.formErrors = response.data.errors;
            }
        );
    };

    $scope.acceptAnswer = function(answer) {
        var url = '/questions/' + $routeParams.id + '/answers/' + answer.id
        $http({ method: 'patch', url: url, data: answer }).
        then(function(response) {
            $route.reload();
        }, function(response) {
            $scope.formErrors = response.data.errors;
        });
    }

    $scope.canAccept = function(question, answer) {
        if ($scope.sharedData.isSigned() && question.user_id == $scope.sharedData.getSignedUser().id && answer.user_id != $scope.sharedData.getSignedUser().id) {
            return true;
        }
        return false;
    }

    $scope.questionHasAcceptedAnswer = function(question) {
        var hasAccepted = false;
        angular.forEach(question.answers, function(answer) {
            if (answer.accepted) {
                hasAccepted = true;
            }
        }, null);   
        return hasAccepted;
    }
});