controllers.controller('UsersController', function ($rootScope, $scope, $routeParams, $window, $location, FileUploader, Api, SharedData) {
	$scope.sharedData = SharedData;
    $scope.user = {};
	$scope.users = [];
    $scope.formError = null;
    $scope.formErrors = null;
    $scope.uploader = new FileUploader({
    	url: '/users/' + $routeParams.id + '/avatars.json'
    });

    $scope.getUsers = function () {
        Api.Users.query(
            function(response, headers) {
                $scope.users = response;
            },
            function(response) {
                console.log("error getting user");
            }
        );
    };

    $scope.getUser = function () {
        Api.Users.get({ id: $routeParams.id },
            function(response, headers) {
                $scope.user = response;
            },
            function(response) {
                console.log("error getting user");
            }
        );
    };

    $scope.updateUser = function () {
        Api.Users.update({ id: $routeParams.id }, $scope.user,
            function(response, headers) {
                $window.location.reload();
            },
            function(response) {
                $scope.formError = response.data.error;
                $scope.formErrors = response.data.errors;
            }
        );
    };

	$scope.isSignedUser = function() {
		return $scope.sharedData.isSigned() && $routeParams.id == $scope.sharedData.getSignedUser().id;
	}

	$scope.setUser = function() {
		$scope.user = $scope.sharedData.getSignedUser();
	}

    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
                $window.location.reload();
    };
});