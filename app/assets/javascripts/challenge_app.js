var challengeApp = angular.module('challengeApp', ['templates', 'ngRoute', 'ngResource', 'controllers', 'angularFileUpload', 'Devise', 'compareField', 'ngMessages']).
    run(function($rootScope, $location) {
        $rootScope.isActive = function (path) {
            if ($location.path() === path) {
                return 'active';
            } else {
                return '';
            }
        };

        $rootScope.errorMessages = [
            {
                type: 'required',
                description: 'This filed is required'
            },
            {
                type: 'minlength',
                description: 'Value is too short in length.'
            },
            {
                type: 'maxlength',
                description: 'Value is too long in length.'
            },
            {
                type: 'pattern',
                description: 'Value of this field is invalid'
            },
            {
                type: 'errorCompareTo',
                description: 'Value has to match previous field'
            },
            {
                type: 'minlength',
                description: 'Value of this field is too short'
            }
        ];

        $rootScope.email_pattern = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
});

var controllers = angular.module('controllers', []);

challengeApp.config(function ($routeProvider) {
   $routeProvider.
       when('/questions/', { controller: 'QuestionsController', templateUrl: 'questions.html' }).
       when('/questions/:id/', { controller: 'QuestionsController', templateUrl: 'question.html' }).
       when('/users/', { controller: 'UsersController', templateUrl: 'users.html' }).
       when('/users/:id/', { controller: 'UsersController', templateUrl: 'user.html' }).
       otherwise({ redirectTo: '/questions/' });
});

challengeApp.factory('Api', function($resource) {
    return {
        Questions: $resource('/questions/:param/:id.json', {}, {
            update: {
                method: 'PUT'
            }
        }),
        Answers: $resource('/questions/:question_id/answers/:id.json', null),
        AnswerLikes: $resource('/answers/:answer_id/likes/:id.json', null),
        UserLikes: $resource('/users/:user_id/likes/:id.json', null),
        Users: $resource('/users/:id.json', {}, {
            update: {
                method: 'PUT'
            }            
        })
    };
});

challengeApp.factory('SharedData', function () {
    var signedUser = { };

    return {
        getSignedUser: function () {
            return signedUser;
        },
        setSignedUser: function (newSignedUser) {
            signedUser = (newSignedUser) ? newSignedUser : {};
        },
        isSigned: function (){
            return Object.keys(signedUser).length > 0;
        }
    };
});

challengeApp.filter('capitalize', function() {
    return function(input, scope) {
        if (input != null) {
            input = input.toLowerCase();
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
});

angular.module('challengeApp').filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
});

challengeApp.filter('nameOrAnonymous', function() {
    return function(input, scope) {
        var value = (input) ? input : 'Anonymous';
        return value;
    }
});