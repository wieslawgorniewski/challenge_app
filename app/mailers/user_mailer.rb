class UserMailer < ApplicationMailer

  def question_new_answer(question)
    @question = question
    mail(to: @question.user.email, subject: t('question.email.new_answer_title'))
  end

  def answer_accepted(answer)
    @answer = answer
    mail(to: @answer.user.email, subject: t('answer.email.answer_accepted_title'))
  end
end
