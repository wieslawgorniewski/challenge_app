class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.config.challange_app_default_email_username
end