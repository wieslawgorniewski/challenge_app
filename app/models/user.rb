class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable
  # :recoverable, :rememberable and :trackable
  POINTS_SUPERSTAR = 1000
  POINTS_INITIAL_BONUS = 100
  POINTS_ANSWER = 25
  POINTS_LIKE = 5
  POINTS_QUESTION_COST = 10
  devise :database_authenticatable, :registerable, :validatable
  
  has_many :questions
  has_many :answers
  has_many :likes

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/system/users/avatars/missing/:style/default.jpg"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  validates :name, length: { minimum: 3, maxiumum: 25 }, allow_blank: true

  before_create :set_initial_points

  def as_json(options={})
      super.as_json(options).merge({avatar_thumb_url: avatar_thumb_url, superstar_badge: superstar_badge})
  end

  def to_s
    name
  end

  def avatar_thumb_url
    self.avatar.url(:thumb)
  end

  def superstar_badge
    self.points >= POINTS_SUPERSTAR
  end  

  def add_points(points)
    self.points += points
    self.save
  end  

  def remove_points(points)
    if (self.points - points) >= 0
      self.points -= points
      self.save
    end
  end  

  private
    def set_initial_points
      self.points = POINTS_INITIAL_BONUS
      true
    end
end
