class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :user
  has_many :likes

  validates :contents, :presence => true

  before_create :set_as_not_accepted, :allow_create_if_question_has_no_answers

  def likes_count
    self.likes.count
  end

  def accept
    if accepted_exists?
      return false
    end  
    self.accepted = true
    self.save
    self.user.add_points(User::POINTS_ANSWER)
    true
  end

  private
    def set_as_not_accepted
      self.accepted = false
      nil
    end

    def allow_create_if_question_has_no_answers
      if accepted_exists?
        self.errors.add(:base, I18n.t('answer.accepted_already_exists_error'))
        return false
      end
      true
    end

    def accepted_exists?
      Answer.where(:question => self.question_id).where(:accepted => true).exists?
    end  
end
