class Like < ActiveRecord::Base
  belongs_to :answer
  belongs_to :user

  validates :answer, :presence => true
  validates :user, :presence => true
  validates :answer_id, uniqueness: { scope: :user_id }

  before_create :reward_answer_creator_with_points
  before_destroy :take_points_away

  private
    def reward_answer_creator_with_points
      self.answer.user.add_points(User::POINTS_LIKE)
      nil
    end

    def take_points_away
      self.answer.user.remove_points(User::POINTS_LIKE)
      nil
    end
end
