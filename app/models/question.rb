class Question < ActiveRecord::Base
  belongs_to :user
  has_many :answers

  validates :title, :presence => true

  before_create :create_if_user_has_enough_points, :remove_user_points

  private
    def create_if_user_has_enough_points
      if self.user.points < User::POINTS_QUESTION_COST
        self.errors.add(:base, I18n.t('question.not_enough_points_error'))
        return false
      end
      true
    end

    def remove_user_points
      self.user.remove_points(User::POINTS_QUESTION_COST)
      true
    end
end
