class AnswersController < ApplicationController
  before_action :authenticate_user!, except: [:new]
  before_action :set_question
  before_action :set_answer, only: [:accept]
  before_action only: [:accept] do
    check_resource_owner!(current_user, @answer.question.user)
  end
  before_action only: [:accept] do
    check_resource_owner!(current_user, @answer.user, :expected => false, :status => 403)
  end

  def create
    @answer = Answer.new(answer_params)
    @answer.user = current_user
    @answer.question = @question
    if @answer.save
      UserMailer.question_new_answer(@question).deliver_later
      respond_to do |format|
        format.json { render :json => { message: t('answer.create_success') }.to_json, :status => 201 }
      end
    else
      respond_to do |format|
        format.json { render :json => { :errors => @answer.errors.full_messages }, :status => 400 }
      end
    end
  end

  def new
    respond_to do |format|
      format.json { render :json => Answer.new }
    end
  end

  def accept
    if @answer.accept
      UserMailer.answer_accepted(@answer).deliver_later
      respond_to do |format|
        format.json { render :json => { message: t('answer.update_success') }.to_json }
      end
    else 
      respond_to do |format|
         format.json { render :json => { :errors => @answer.errors.full_messages }, :status => 400 }
      end
    end
  end

  private

    def set_question
      @question = Question.find(params[:question_id])
    end

    def set_answer
      @answer = Answer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:contents, :accepted)
    end
end
