class QuestionsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy]
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_action only: [:update, :destroy] do
    check_resource_owner!(current_user, @question.user)
  end

  def index
    respond_to do |format|
      format.json { render :json => Question.all.to_json(:include => { :user => { :only => [:name ]} }) }
    end
  end

  def show
    respond_to do |format|
      format.json { 
        render :json => @question.to_json(
          :include => { 
            :answers => { 
              :methods => [:likes_count] 
            },
            :user => { 
              :only => [:name],
              :methods => [:avatar_thumb_url]
            },
          }
        ) 
      }
    end
  end

  def new
    respond_to do |format|
      format.json { render :json => Question.new }
    end
  end

  def edit
    respond_to do |format|
      format.json { render :json => @question.to_json }
    end
  end

  def create
    @question = Question.new(question_params)
    @question.user = current_user

    if @question.save
      respond_to do |format|
        format.json { render :json => { message: t('question.create_success') }.to_json, :status => 201 }
      end
    else
      respond_to do |format|
         format.json { render :json => { :errors => @question.errors.full_messages }, :status => 400 }
      end
    end
  end

  def update
    if @question.update(question_params)
      respond_to do |format|
        format.json { render :json => { message: t('question.update_success') }.to_json }
      end
    else
      respond_to do |format|
         format.json { render :json => { :errors => @question.errors.full_messages }, :status => 400 }
      end
    end
  end

  def destroy
    @question.destroy
    respond_to do |format|
      format.json { render :json => { message: t('question.delete_success') }.to_json, :status => 204 }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_question
    @question = Question.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def question_params
    params.require(:question).permit(:title, :contents)
  end
end
