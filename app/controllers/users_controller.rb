class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:update, :avatar]
  before_action :set_user, except: [:index]
  before_action only: [:update, :avatar] do
    check_resource_owner!(current_user, @user)
  end
  skip_before_action :verify_authenticity_token, only: [:avatar]

  def index
    respond_to do |format|
      format.json { render :json => User.limit(10).order(points: :desc) }
    end
  end

  def show
    respond_to do |format|
      format.json { render :json => @user }
    end
  end

  def update
    if @user.update(user_params)
      respond_to do |format|
        format.json { render :json => { message: t('user.update_success') }.to_json }
      end
    else
      respond_to do |format|
         format.json { render :json => { :errors => @user.errors.full_messages }, :status => 400 }
      end
    end
  end

  def avatar
    begin
      if params[:file] and @user.update(:avatar => params[:file])
        respond_to do |format|
          format.json { render :json => { message: t('user.update_success') }.to_json }
        end
      else
        respond_to do |format|
          format.json { render :json => { :errors => @user.errors.full_messages }, :status => 400 }
        end
      end
    rescue Paperclip::AdapterRegistry::NoHandlerError
      puts 'They are hakkking us.' # Log somewhere that we are being shamefully hacked, most probably by Django developers
      respond_to do |format|
        format.json { render :json => { :errors => @user.errors.full_messages }, :status => 400 }
      end
    end  
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :password, :name)
    end
end
