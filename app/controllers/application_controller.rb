class ApplicationController < ActionController::Base
  respond_to :html, :json
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :default_format_json

  # Not sure if this is correct approach
  rescue_from ActiveRecord::RecordNotFound do
    render nothing: true, status: 404
  end

  rescue_from ActionController::ParameterMissing do
    render :nothing => true, :status => 400
  end

  def default_format_json
    request.format = 'json' unless params[:format]
  end

  def check_resource_owner!(resource_1, resource_2, options = {})
    expected = true
    if options.has_key?(:expected)
      expected = options[:expected] 
    end 
    status = options[:status] || 401

    if (resource_1.id == resource_2.id) != expected
      respond_to do |format|
        format.json { render :json => { :error => t('common.check_resource_owner_error') }, :status => status }
      end
    end  
  end  
end
