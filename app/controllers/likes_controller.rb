class LikesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_answer, except: [:index_user_likes]
  before_action :set_user, only: [:index_user_likes] 
  before_action :set_like, only: [:destroy]
  before_action only: [:create] do
    check_resource_owner!(current_user, @answer.user, :expected => false, :status => 403)
  end

  def index
    respond_to do |format|
      format.json { render :json => @answer.likes }
    end
  end

  def index_user_likes
    respond_to do |format|
      format.json { render :json => @user.likes }
    end
  end  

  def new
    respond_to do |format|
      format.json { render :json => Like.new.to_json }
    end
  end

  def create
    @like = Like.new
    @like.answer = @answer
    @like.user = current_user

    if @like.save
      respond_to do |format|
        format.json { render :json => { message: t('like.create_success') }.to_json, :status => 201 }
      end
    else
      respond_to do |format|
         format.json { render :json => { :errors => @like.errors.full_messages }, :status => 400 }
      end
    end
  end

  def destroy
    if @like.user_id == current_user.id and @like.destroy
      respond_to do |format|
        format.json { render :json => { :message => t('like.delete_success') }.to_json, :status => 204 }
      end
    else
      respond_to do |format|
         format.json { render :json => { :errors => t('like.delete_error') }.to_json, :status => 401 }
      end
    end  
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_answer
    @answer = Answer.find(params[:answer_id])
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def set_like
    @like = Like.find(params[:id])
  end
end
