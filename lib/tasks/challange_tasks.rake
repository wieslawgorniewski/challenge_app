task :add_initial_bonus_points_to_existing_users => :environment do
  User.where(:points => nil).update_all(:points => User::POINTS_INITIAL_BONUS)
end